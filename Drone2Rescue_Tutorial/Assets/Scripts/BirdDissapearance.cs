﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdDissapearance : MonoBehaviour {


    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Finish")
        {
            Destroy(other.gameObject);
        }
    }
}
