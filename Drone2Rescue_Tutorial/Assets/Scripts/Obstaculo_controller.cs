﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstaculo_controller : MonoBehaviour {

    public float speed;
    public float semiamplitud_mov;
    private Rigidbody rb;
    private Vector3 i_coords;
    private bool mov_up; // Nos permite ver direccion de movimiento

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        i_coords = GetComponent<Transform>().position;
        mov_up = true;
	}
    // Esta hecho provando una corrutina. Sino quitar esto y el update y dejar solo el fixedUpdate q tbn funciona
    // No se si esta bien usada porq sin el time.deltatime no funciona..
    IEnumerator Oscilacion()
    {
        if((GetComponent<Transform>().position[1] <= (i_coords[1] + semiamplitud_mov)) && mov_up)
        {
            transform.Translate(0f, 0.5f * speed * Time.deltaTime, 0f);
            if (GetComponent<Transform>().position[1] >= (i_coords[1] + semiamplitud_mov))

            { //hemos llegado lim superior del movimiento
                mov_up = false;
            }
            // movement *= Time.deltaTime;
            //rb.AddForce(movement * speed);
            yield return null;
        }
        if ((GetComponent<Transform>().position[1] > (i_coords[1] - semiamplitud_mov)) && !mov_up)
        {
            transform.Translate(0f, -0.5f * speed * Time.deltaTime, 0f);
            if (GetComponent<Transform>().position[1] < (i_coords[1] - semiamplitud_mov)) //hemos llegado lim inferior del movimiento
            {
                mov_up = true;
            }
            //movement *= Time.deltaTime;
            //rb.AddForce(movement * speed);
            yield return null;
        }
    }

    /*  private void FixedUpdate()
      {
          if ((GetComponent<Transform>().position[1] <= (i_coords[1] + semiamplitud_mov)) && mov_up)
          {
              transform.Translate(0f, 0.5f * speed * Time.deltaTime, 0f);
              if (GetComponent<Transform>().position[1] >= (i_coords[1] + semiamplitud_mov)) { //hemos llegado lim superior del movimiento
                  mov_up = false;
              }
             // movement *= Time.deltaTime;
              //rb.AddForce(movement * speed);

          }
          if ((GetComponent<Transform>().position[1] > (i_coords[1] - semiamplitud_mov)) && !mov_up)
              {
              transform.Translate(0f, -0.5f * speed * Time.deltaTime, 0f);
              if (GetComponent<Transform>().position[1] < (i_coords[1] - semiamplitud_mov)) //hemos llegado lim inferior del movimiento
              {
                  mov_up = true;
              }
              //movement *= Time.deltaTime;
              //rb.AddForce(movement * speed);

          }
      }*/
    private void Update()
    {
        StartCoroutine("Oscilacion");
    }



}
