﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{

    public float speed;
    public float semiamplitud_mov;
    private Vector3 i_coords;
    private bool mov_up; // Nos permite ver direccion de movimiento

    // Use this for initialization
    void Start()
    {

        i_coords = GetComponent<Transform>().position;
        mov_up = true;
    }

    private void FixedUpdate()
    {
        if ((GetComponent<Transform>().position[1] <= (i_coords[1] + semiamplitud_mov)) && mov_up)
        {
            transform.Translate(0f, 0.5f * speed * Time.deltaTime, 0f);
            if (GetComponent<Transform>().position[1] >= (i_coords[1] + semiamplitud_mov))
            { //hemos llegado lim superior del movimiento
                mov_up = false;
            }

        }
        
        else
        {
            transform.position = i_coords;
            mov_up = true;
            
        }
       
    }
}
