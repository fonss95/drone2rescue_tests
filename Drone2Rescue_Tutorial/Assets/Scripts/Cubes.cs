﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Cubes : MonoBehaviour {

    public int NumberOfCube=0;
    static int cont=1;
    float t = 0f;
    int scoreValue = 10;

    Rigidbody rb;
    Material m;
    Light l;
    

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        m = GetComponent<Renderer>().material;
        l = GetComponent<Light>();
    }

    void Update ()
    {
        t += Time.deltaTime;
        if (cont == NumberOfCube)
        {
            l.intensity = 3;
            m.color = Color.red;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (t >= 1 && cont==NumberOfCube)
        {
            l.intensity = 0;
            m.color = new Color(149 / 255f, 71 / 255f, 109 / 255f);
            cont++;
            ScoreManager.score += scoreValue;
        }
    }
}
